# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 17:10:50 2021

@author: jam37
This script is used to receive a CW transmission
"""

import SoapySDR
import time
import numpy as np
from matplotlib import pyplot as plt
from SoapySDR import * #SOAPY_SDR_constants
from scipy.fft import fft, fftfreq, fftshift
#Now for the parameters of the CW transmission
serial = "VG2B000013"
chan = 0
rate = 10e6 #10 Msps
rxGain = 10
freq = 2.451e9 #RF Frequency of 2.49 GHz
delay = int(30e6) #Tx hardware setup delay in ns
nsamps = 8092 #Number of samples


# Init Rx
sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
sdr.setSampleRate(SOAPY_SDR_RX, 0, rate)
sdr.setFrequency(SOAPY_SDR_RX, 0, "RF", freq)
sdr.setGain(SOAPY_SDR_RX, 0, rxGain)
sdr.setAntenna(SOAPY_SDR_RX, 0, "RX")

#Init buffers
sampsRecv = np.zeros(nsamps, dtype=np.complex64)

#Init Streams
rxStream = sdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, [0], {})
ts = sdr.getHardwareTime() + delay
sdr.activateStream(rxStream, SOAPY_SDR_HAS_TIME, ts)

t=time.time()
total_samps = 0

#Receive one packet
sr = sdr.readStream(rxStream, [sampsRecv], nsamps, timeoutUs= delay)    
if sr.ret != nsamps:
	print("Bad Read!!!")

#print an update
total_samps += nsamps
print("Total Samples Received: %i" % total_samps)
#do some rx processing here

#Plot I/Q in time domain
plt.figure()
plt.plot(np.real(sampsRecv))
plt.plot(np.imag(sampsRecv))
plt.show()

#Plot I/Q constellation
plt.figure()
plt.scatter(np.real(sampsRecv), np.imag(sampsRecv))
plt.show()

#Plot FFT
plt.figure()
fVals = np.arange(start = -nsamps/2,stop = nsamps/2)*rate/nsamps
yf = fftshift(fft(sampsRecv,nsamps))
plt.plot(fVals,np.abs(yf),'b')
plt.show()