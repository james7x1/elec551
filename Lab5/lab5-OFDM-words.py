# -*- coding: utf-8 -*-
"""
Created on Sat Nov 20 16:29:12 2021

@author: jam37
This script performs a full OFDM transmission using DPSK modulation (other
modulations supported, but I have not programmed the demodulation)
"""
import SoapySDR
import scipy
import time
import signal
import sys
import os
import random
import numpy as np
from matplotlib import pyplot as plt
from scipy.fft import fft, fftfreq, fftshift
from scipy import io
from SoapySDR import * #SOAPY_SDR_constants

#Constants from 802.11 OFDM
serial = sys.argv[1]
mode = sys.argv[2] #tx/rx
gain = int(sys.argv[3]) #The amplitude gain for the reciever
modType = sys.argv[4] #QAM, PSK, DPSK
modOrder = int(sys.argv[5]) #4, 8, 16, 64
chan = 0
rate = 20e6 #Digital Sampling Frequency
freq = 2.41e9 #RF Frequency of 2.49 GHz
delay = int(30e6) #Tx hardware setup delay in ns
alpha = 0.9564 #In-phase IQ imbalance coefficient
beta = 1.0 #Quadrature IQ imbalance coefficient
resampleFactor = 8 #Upsample tx side, Downsample rx side
digitalIF = rate/4 #Digital IF for Up/DownConversion
digitalRate = rate*resampleFactor #New digital sampling rate DISABLE if not upsampling!
sts_freq = np.array([0,0,0,0,0,0,0,0,1+1j,0,0,0,-1-1j,0,0,0,1+1j,0,0,0,-1-1j,0,0,0,-1-1j,0,0,0,1+1j,0,0,0,0,0,0,0,-1-1j,0,0,0,-1-
1j,0,0,0,1+1j,0,0,0,1+1j,0,0,0,1+1j,0,0,0,1+1j,0,0,0,0,0,0,0]) #copied from lecture notes
lts_freq = np.array([0,0,0,0,0,0,1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,0,1,-1,-1,1,1,-1,1,-1,1,-1,-1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,1,1,1,1,0,0,0,0,0]) #copied from lts.py
LTSThresh = 0.8
pilotCarriers = [ 0,  1,  2,  3,  4,  5, 32, 59, 60, 61, 62, 63] #indexs for zeroed subcarriers
dataCarriers = [ 6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58] #Indexs for data subcarriers
payloadBits_perOFDM = 2*52 #Number of bits/symbol * symbols per ODFM symbol aka len(dataCarriers)

#bitsBuffer = 19320233934445550442255480640130 #bin(bitsBuffer) is the data to be sent. This is a 104 bit integer with 0.5 probability
bitsBuffer = ''.join(format(i, '08b') for i in bytearray('Hello, my name is Allen!!! Where is my IEEE Merch???', encoding ='utf-8')) #For the sake of simplicity, the string must be multiple of 26 characters
numOFDMSymbols = 4 #Hardcoded, see line 171 for derivation. Assumes 4 DPSK

def genSTS(cp = 32):
    '''The genSTS function creates a time-domain 802.11 STS using the frequency values of the sequence and a cyclic prefix of "cp" (32). Modified from LTS.py'''
    signal = np.fft.ifft(np.fft.ifftshift(sts_freq))
    signal = signal/np.absolute(signal).max()*0.5  #normalize to 0.5
    signal = np.concatenate((signal[len(signal) - cp:], signal, signal)) #apply cyclic-prefix and repeat
    
    return signal    

def genLTS(upsample=1, cp=32):
    '''Generate a time-domain 802.11 LTS with a cyclic prefix of "cp" (32) and upsampled by a factor of "up" (1). Copied from LTS.py'''
    up_zeros = np.zeros(len(lts_freq)//2*(upsample-1))
    lts_freq_up = np.concatenate((up_zeros,lts_freq,up_zeros))
    signal = np.fft.ifft(np.fft.ifftshift(lts_freq_up))
    signal = signal/np.absolute(signal).max()*0.5  #normalize to 0.5
    
    #Now affix the cyclic prefix
    signal = np.concatenate((signal[len(signal) - cp:], signal, signal))  #could use tile...

    return signal
    
def modulate(data, modType, modOrder):
    '''The modulate function takes in the bytes of data, the type of modulation, and the order of modulation. It maps symbol of data to the correct complex value and passes it out as modulatedData '''
    #Create the modulation mapping array
    if modType == "QAM":
        if modOrder == 4:
            modMap = [-0.5+0.5j, -0.5-0.5j,  0.5+0.5j,  0.5-0.5j]
            #2 bits/symbol, so the shift is
            shift = 2
        if modOrder == 16:
            modMap = [-0.5       +0.5j       , -0.5       +0.16666667j,
        -0.5       -0.5j       , -0.5       -0.16666667j,
        -0.16666667+0.5j       , -0.16666667+0.16666667j,
        -0.16666667-0.5j       , -0.16666667-0.16666667j,
         0.5       +0.5j       ,  0.5       +0.16666667j,
         0.5       -0.5j       ,  0.5       -0.16666667j,
         0.16666667+0.5j       ,  0.16666667+0.16666667j,
         0.16666667-0.5j       ,  0.16666667-0.16666667j]
            #4 bits/symbol, so the shift is
            shift = 4
        if modOrder == 64:
            modMap = [-0.5       +0.5j       , -0.5       +0.35714286j,
        -0.5       +0.07142857j, -0.5       +0.21428571j,
        -0.5       -0.5j       , -0.5       -0.35714286j,
        -0.5       -0.07142857j, -0.5       -0.21428571j,
        -0.35714286+0.5j       , -0.35714286+0.35714286j,
        -0.35714286+0.07142857j, -0.35714286+0.21428571j,
        -0.35714286-0.5j       , -0.35714286-0.35714286j,
        -0.35714286-0.07142857j, -0.35714286-0.21428571j,
        -0.07142857+0.5j       , -0.07142857+0.35714286j,
        -0.07142857+0.07142857j, -0.07142857+0.21428571j,
        -0.07142857-0.5j       , -0.07142857-0.35714286j,
        -0.07142857-0.07142857j, -0.07142857-0.21428571j,
        -0.21428571+0.5j       , -0.21428571+0.35714286j,
        -0.21428571+0.07142857j, -0.21428571+0.21428571j,
        -0.21428571-0.5j       , -0.21428571-0.35714286j,
        -0.21428571-0.07142857j, -0.21428571-0.21428571j,
         0.5       +0.5j       ,  0.5       +0.35714286j,
         0.5       +0.07142857j,  0.5       +0.21428571j,
         0.5       -0.5j       ,  0.5       -0.35714286j,
         0.5       -0.07142857j,  0.5       -0.21428571j,
         0.35714286+0.5j       ,  0.35714286+0.35714286j,
         0.35714286+0.07142857j,  0.35714286+0.21428571j,
         0.35714286-0.5j       ,  0.35714286-0.35714286j,
         0.35714286-0.07142857j,  0.35714286-0.21428571j,
         0.07142857+0.5j       ,  0.07142857+0.35714286j,
         0.07142857+0.07142857j,  0.07142857+0.21428571j,
         0.07142857-0.5j       ,  0.07142857-0.35714286j,
         0.07142857-0.07142857j,  0.07142857-0.21428571j,
         0.21428571+0.5j       ,  0.21428571+0.35714286j,
         0.21428571+0.07142857j,  0.21428571+0.21428571j,
         0.21428571-0.5j       ,  0.21428571-0.35714286j,
         0.21428571-0.07142857j,  0.21428571-0.21428571j]
            #6 bits/symbol, so the shift is
            shift = 6
    if modType == "PSK":
        if modOrder == 8:
            modMap = [ 5.00000000e-01+0.00000000e+00j,  3.53553391e-01+3.53553391e-01j,
         3.06161700e-17+5.00000000e-01j, -3.53553391e-01+3.53553391e-01j,
        -5.00000000e-01+6.12323400e-17j, -3.53553391e-01-3.53553391e-01j,
        -9.18485099e-17-5.00000000e-01j,  3.53553391e-01-3.53553391e-01j]
            #3 bits/symbol
            shift = 3
        if modOrder == 16:
            modMap = [ 5.00000000e-01+0.00000000e+00j,  4.61939766e-01+1.91341716e-01j,
         3.53553391e-01+3.53553391e-01j,  1.91341716e-01+4.61939766e-01j,
         3.06161700e-17+5.00000000e-01j, -1.91341716e-01+4.61939766e-01j,
        -3.53553391e-01+3.53553391e-01j, -4.61939766e-01+1.91341716e-01j,
        -5.00000000e-01+6.12323400e-17j, -4.61939766e-01-1.91341716e-01j,
        -3.53553391e-01-3.53553391e-01j, -1.91341716e-01-4.61939766e-01j,
        -9.18485099e-17-5.00000000e-01j,  1.91341716e-01-4.61939766e-01j,
         3.53553391e-01-3.53553391e-01j,  4.61939766e-01-1.91341716e-01j]
            #4 bits/symbol
            shift = 4
    if modType == "DPSK":
        if modOrder == 4:
            #|C    A|
            #|D    B|
            #00 = 0
            #01 = +pi/2
            #10 = -pi/2
            #11 = +pi
            #2 bits/symbol, so the shift is
            shift = 2
    #Now break up the data in symbols sizes and map it to the symbol values
    numberOfSymbols = int(np.ceil(len(bitsBuffer)/shift))
    modulatedData = np.zeros(numberOfSymbols,np.complex64)
    for symbolNum in range(0,numberOfSymbols) :
        if modType == "DPSK":
            if symbolNum == 0:
                #Assume A is the first arbitary symbol sent
                modIndex = int(bitsBuffer[symbolNum*shift:symbolNum*shift+shift].ljust(shift,'0'),2)
                modulatedData[symbolNum] = DPSK_0Switch(modIndex)
            else:    
                modIndex = int(bitsBuffer[symbolNum*shift:symbolNum*shift+shift].ljust(shift,'0'),2)
                modulatedData[symbolNum] = DPSK_Switch(modIndex,modulatedData[symbolNum-1])
        else:
            modIndex = int(bitsBuffer[symbolNum*shift:symbolNum*shift+shift].ljust(shift,'0'),2)
            modulatedData[symbolNum] = modMap[modIndex];
        
    return modulatedData

def OFDM_Modulate(modSymbols):
    numOFDMSymbols = int(np.ceil(len(modSymbols)/len(dataCarriers))) #Also hardcoded at top
    for i in range(0,numOFDMSymbols):
        #Set up OFDM Symbol
        OFDM_Symbol = np.zeros(64, dtype=np.complex64())
        OFDM_Symbol[pilotCarriers] = 0 #zeroing subcarriers
        #dataSymbols = np.zeros(len(dataCarriers),dtype=np.complex128())
        OFDM_Symbol[dataCarriers] = modulatedSig[i*len(dataCarriers):i*len(dataCarriers)+len(dataCarriers)] #Inset next data symbols
        #iFFT for OFDM
        OFDM_Symbol_time = np.fft.ifft(OFDM_Symbol)
        #Add CP to symbol, making the OFDM symbol 96 symbols long
        OFDM_Symbol_time = np.concatenate((OFDM_Symbol_time[-32:],OFDM_Symbol_time))#Append the last 32 symbols to the front as CP
        OFDM_Symbol_time = OFDM_Symbol_time/OFDM_Symbol_time.max()*0.5 #Normalize to 0.5
        if i == 0:
            OFDM_time = OFDM_Symbol_time
        else:
            #Concatenate to OFDM frame
            OFDM_time = np.concatenate((OFDM_time,OFDM_Symbol_time))
        
    return OFDM_time
    
def DPSK_0Switch(argument):
    switcher = {
        0: np.array([0.5+0.5j]),
        1: np.array([-0.5+0.5j]),
        2: np.array([0.5-0.5j]),
        3: np.array([-0.5-0.5j])
    }
    return switcher.get(argument, "Invalid symbol")

def DPSK_Switch(argument,lastSymbol):
    switcher = {
        0: lastSymbol,
        1: lastSymbol*np.exp(1j*np.pi/2),
        2: lastSymbol*np.exp(1j*-np.pi/2),
        3: lastSymbol*np.exp(1j*np.pi)
    }
    return switcher.get(argument, "Invalid symbol")

def OFDM_Demodulate(symbols):
    #Remove CP
    symbols = symbols[32::]
    #FFT for OFDM
    symbols = fft(symbols)
    #Retrieve the data Carrier symbols
    symbols = symbols[dataCarriers]
    #Equalization
    #symbols = symbols/CSI[dataCarriers]
    
    return symbols

def DPSK_Demodulate(angle):
    if -np.pi/4<angle<np.pi/4:
        return 0
    if np.pi/4<angle<3*np.pi/4:
        return 1
    if -3*np.pi/4<angle<-np.pi/4:
        return 2
    if 3*np.pi/4<angle<np.pi or -np.pi<angle<-3*np.pi/4:
        return 3

def softShutdown(signum, frame):
    print("Cleaning up streams")
    sdr.deactivateStream(txStream)
    sdr.closeStream(txStream)
    print("Done!")
    sys.exit(0)

#Checking Arguments
if len(sys.argv) != 6:
        print("Usage:python %s IrisSerial, tx/rx, gain, modulation type, order" % os.path.basename(__file__))
        sys.exit(-1)

if mode == "tx":
    #Modulate signal
    modulatedSig = modulate(bitsBuffer,modType,modOrder)
    
    #Transform to OFDM Symbols
    OFDM_Symbols = OFDM_Modulate(modulatedSig)
    
    #Add LTS
    print("There are %d data carrying symbols" % len(modulatedSig))
    withLTS_sig =  np.concatenate((genLTS(),OFDM_Symbols))
    
    #Repeat the signal to make it large enough to fit on ethernet
    withLTS_sig = np.tile(withLTS_sig,4)
    
    #Declare with signal to transmit
    sig = withLTS_sig
    nsamps = len(sig)
    print("Number of Samples = %d" %nsamps)
    
    #Cross-Correlation for LTS
    LTScorr = np.correlate(sig,genLTS()[32:64+32],'full')
    LTScorrNorm = np.abs(LTScorr.real)/np.abs(LTScorr.real).max() #Normalize correlation
    #Find index of the first peak and correct for zero padding to get signal index of the start of LTS
    #firstPeak = np.nonzero(LTScorr>=LTSThresh)[0][0]-64
    firstPeak = LTScorrNorm.argmax()-64 #provides index in signal
    # peaks = np.nonzero(LTScorrNorm>=LTSThresh)[0] #index of peaks in correlation
    # if peaks[1]-peaks[0] == 64:
    #     firstPeak = peaks[0]-64 #index of beginning of LTS in signal
    # else:
    #     firstPeak = peaks[1]-64
    #CFOaccum = 0
    # for i in range(0,64):
    #     CFOaccum = CFOaccum + (np.angle(sig[firstPeak+64+i])-np.angle(sig[firstPeak+i]))
    # CFOavg = CFOaccum/64
    CFO = (np.angle(sig[firstPeak+64])-np.angle(sig[firstPeak]))
    print(CFO*rate/(2*np.pi*64))
    #Correct for sample accurate timing by making the beginning of the LTS the start of the buffer through truncation
    #LTScorrNorm = LTScorrNorm[firstPeak+64:]
    
    #Plot Cross-Correlation of LTS
    plt.figure()
    plt.plot(LTScorrNorm)
    plt.title("LTS Cross-Correlation")
    plt.show()
    
    #DEBUGGING: Plotting for Transmitted Signal
    #Plot Time-Domain
    plt.figure()
    plt.title('Time-Domain of Transmitted Signal')
    plt.xlabel('Sample Number')
    plt.ylabel('Sample Value')
    plt.plot(sig.real,label = "Real")
    plt.plot(sig.imag,label = "Imaginary")
    plt.legend(loc="upper right")
    plt.show()
    
    #Plot Constellation
    plt.figure()
    plt.scatter(sig.real,sig.imag)
    plt.title("Transmitted Signal Constellation")
    plt.xlabel("In-Phase")
    plt.ylabel('Quadrature')
    plt.show()
    
    #Plot PSD
    plt.figure()
    plt.title('PSD of Transmitted Signal')
    plt.psd(sig,len(sig),rate)
    plt.show()
    
    
    # #Simulated
    
    # #Get CSI from the LTS
    # firstLTS = sig[firstPeak:firstPeak+64]
    # secondLTS = sig[firstPeak+64:firstPeak+128]
 
    # yf1 = fftshift(fft(firstLTS,64)) #FFT of firstCSI
    # yf2 = fftshift(fft(secondLTS,64)) #FFT of secondCSI
    # CSI  = ((yf1+yf2)/2)*lts_freq #Average the FFTs and create channel estimates
    # oneRep = yf1/CSI
    
    # #Plot I/Q constellation
    # plt.figure()
    # plt.title("Constellation of 1 rep")
    # plt.xlabel("In-Phase")
    # plt.ylabel("Quadrature")
    # plt.scatter(oneRep.real, oneRep.imag)
    # plt.show() 
    
    # #Plot Channel Estimates MAG
    # plt.figure()
    # fVals = np.arange(start = -64/2,stop = 64/2)*rate/64
    # plt.plot(fVals, np.abs(CSI),'b')
    # plt.xlabel("Frequency (Hz)")
    # plt.ylabel("Amplitude")
    # plt.title("Amplitude of Channel Estimates")
    # plt.show()
    
    # #Plot Channel Estimate ANG
    # plt.figure()
    # fVals = np.arange(start = -64/2,stop = 64/2)*rate/64
    # plt.plot(fVals, np.angle(CSI),'b')
    # plt.xlabel("Frequency (Hz)")
    # plt.ylabel("Phase (Radians)")
    # plt.title("Phase of Channel Estimates")
    # plt.show()
    
    #Get the OFDM Symbols following the LTS
    sig = sig[firstPeak+128:firstPeak+128+96*numOFDMSymbols] #Each OFDM Symbol is 96 symbols long (has 32 bit CP)
    
    #Take each OFDM symbol, remove the CP, demodulate and equalize
    for i in range(0,numOFDMSymbols):
        if i == 0:
            sampsRecvCAT = OFDM_Demodulate(sig[i*96:i*96+96])
        else:
            sampsRecvi = OFDM_Demodulate(sig[i*96:i*96+96])
            sampsRecvCAT = np.concatenate((sampsRecvCAT,sampsRecvi))
    #These are the data symbols
    sig = sampsRecvCAT
    
    #Plot I/Q constellation
    plt.figure()
    plt.title("Constellation of Received Signal")
    plt.xlabel("In-Phase")
    plt.ylabel("Quadrature")
    plt.scatter(sig.real, sig.imag)
    plt.show()
    
    #Demodulation
    demodData = np.zeros(len(sig))
    if modType == "DPSK":
        sig = np.concatenate((np.array([0.5+0.5j]),sig)) #Assume the first phase is A just like in transmission
        for i in range(1,len(sig)) :
            ang = np.angle(sig[i]) - np.angle(sig[i-1])
            if ang > np.pi:
                ang = ang -2*np.pi
            elif ang < -np.pi:
                ang = ang + 2*np.pi
            demodData[i-1] = DPSK_Demodulate(ang)
            #Transform symbols into bits
            demodBits = ''
            for a in demodData:
                demodBits = demodBits + format(int(a),'b').zfill(2)
    
    #Convert the bits back to text from ASCII
    # initializing a empty string for
    # storing the string data
    text =' '
      
    # slicing the input and converting it
    # in decimal and then converting it in string
    for i in range(0, len(demodBits), 8):
        # slicing the bin_data from index range [0, 8]
        # and storing it in temp_data
        temp_data = demodBits[i:i + 8]
          
        # Get decimal value of corresponding temp_data
        decimal_data = int(temp_data,2)
          
        # Decoding the decimal value returned by
        # BinarytoDecimal() function, using chr()
        # function which return the string corresponding
        # character for given ASCII value, and store it
        # in str_data
        text = text + chr(decimal_data)
    
    print("Received Text is: %s" %text)
    
    #BER Counter (We are asked to cheat since we know the data. In real-life
    #we would use integrity measures via a MAC to generate this, or a checksum)
    BER = 0
    for i in range(len(bitsBuffer)):
        if bitsBuffer[i] != demodBits[i]:
            BER += 1
    print("The BER = %d" %BER)
    
    #Init Tx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    sdr.setSampleRate(SOAPY_SDR_TX, chan, rate)
    sdr.setFrequency(SOAPY_SDR_TX, chan, "RF", freq)
    sdr.setGain(SOAPY_SDR_TX, chan, gain)
    #sdr.setAntenna(SOAPY_SDR_TX, chan, "TRX")
    
    #Init Tx Streams
    txStream = sdr.setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32, [0], {})
    ts = sdr.getHardwareTime() + delay #give us delay in nanosec to set everything up.
    txFlags = SOAPY_SDR_HAS_TIME
    sdr.activateStream(txStream)
    
    t=time.time()
    total_samps = 0
    
    #First Transmission needs a time stamp.
    sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), txFlags, ts+delay)
    if sr.ret != len(sig):
            print("Bad Write!!!")
            
    #Catch CTRL-C
    signal.signal(signal.SIGINT, softShutdown)
    
    #continuously transmit until the script is killed
    while True :
        sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), 0)
        if sr.ret != len(sig):
            print("Bad Write!!!")
        
        #print an update
        total_samps += len(sig)
        if time.time() - t > 1:
            t=time.time()
            print("Total Samples Sent: %i" % total_samps)

elif mode == "rx":
    nsamps = 8192 #Number of samples
    # Init Rx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    sdr.setSampleRate(SOAPY_SDR_RX, 0, rate)
    sdr.setFrequency(SOAPY_SDR_RX, 0, "RF", freq)
    sdr.setGain(SOAPY_SDR_RX, 0, gain)
    #sdr.setAntenna(SOAPY_SDR_RX, 0, "TRX")
    
    #Init buffers
    sampsRecv = np.zeros(nsamps, dtype=np.complex64)
    
    #Init Streams
    rxStream = sdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, [0], {})
    ts = sdr.getHardwareTime() + delay
    sdr.activateStream(rxStream, SOAPY_SDR_HAS_TIME|SOAPY_SDR_END_BURST, ts)
    
    t=time.time()
    total_samps = 0
    
    #Receive one packet
    sr = sdr.readStream(rxStream, [sampsRecv], nsamps, timeoutUs= delay)    
    if sr.ret != nsamps:
        print("Bad Read!!!")
    
    #print an update
    total_samps += nsamps
    print("Total Samples Received: %i" % total_samps)
    #do some rx processing here
    
    # #DC Offset Correction
    # sampsRecv = (np.real(sampsRecv)-np.average(np.real(sampsRecv))) + 1j*(np.imag(sampsRecv)-np.average(np.imag(sampsRecv)))
    
    # #IQ Imbalance Correction
    # sampsRecv = alpha*np.real(sampsRecv) + beta*1j*np.imag(sampsRecv)

    # # Induce a CFO
    # sinusoidFreq = 125e3
    # period = int((1/sinusoidFreq)*rate);
    # stime = np.linspace(0,1/sinusoidFreq,len(sampsRecv))
    # complexSinusoid = np.exp(1j*2*np.pi*sinusoidFreq*stime*len(sampsRecv)/period)
    # sampsRecv = sampsRecv*complexSinusoid
    
    # #Auto-Correlation for STS
    # delayed = np.concatenate((np.complex128(np.zeros(16)),sampsRecv))
    # sampsRecv_padded = np.concatenate((sampsRecv,np.complex128(np.zeros(16))))
    # conj_d = np.conj(delayed)
    # multDelay = sampsRecv_padded*conj_d
    # STSaccum = np.zeros(len(multDelay))
    # for i in range(0,len(multDelay)):
    #     STSaccum[i]=np.sum(multDelay[0:i])
        
    #Plot Time-Domain of Signal
    plt.figure()
    plt.plot(sampsRecv.real,label = "Real")
    plt.plot(sampsRecv.imag ,label = "Imag")
    plt.title("Time Domain of Received Signal")
    plt.legend(loc="upper right")
    plt.xlabel("Sample")
    plt.ylabel("Amplitude")
    plt.show()
    
    #Plot FFT
    plt.figure()
    fVals = np.arange(start = -nsamps/2,stop = nsamps/2)*rate/nsamps
    yf = fftshift(fft(sampsRecv,nsamps))
    plt.plot(fVals,np.abs(yf),'b')
    plt.title('FFT of Received Signal')
    plt.show()
    
    #Plot PSD
    plt.figure()
    plt.title("PSD")
    plt.xlabel("Frequency of Received Signal")
    plt.ylabel("Power in dBm")
    [Pxx, freqs] = plt.psd(sampsRecv,len(sampsRecv),rate,0)
    plt.show()
    
    #Cross-Correlation for LTS
    LTScorr = np.correlate(sampsRecv,genLTS()[32:64+32],'full')
    LTScorrNorm = np.abs(LTScorr.real)/np.abs(LTScorr.real).max() #Normalize correlation
    #Find index of the first peak and correct for zero padding to get signal index of the start of LTS
    #firstPeak = np.nonzero(LTScorr>=LTSThresh)[0][0]-64
    #firstPeak = LTScorr.argmax()-64 #provides index in signal
    peaks = np.nonzero(LTScorrNorm>=LTSThresh)[0] #index of peaks in correlation
    if peaks[1]-peaks[0] == 64:
        firstPeak = peaks[0]-64 #index of beginning of LTS in signal
    else:
        firstPeak = peaks[1]-64
    CFOaccum = 0
    # for i in range(0,64):
    #     CFOaccum = CFOaccum + (np.angle(sig[firstPeak+64+i])-np.angle(sig[firstPeak+i]))
    # CFOavg = CFOaccum/64
    CFO = (np.angle(sampsRecv[firstPeak+64])-np.angle(sampsRecv[firstPeak]))
    CFO_out = CFO*rate/(2*np.pi*64)
    print("The CFO = %f" % CFO_out)
    #Correct for sample accurate timing by making the beginning of the LTS the start of the buffer through truncation
    
    #Plot Cross-Correlation of LTS
    plt.figure()
    plt.plot(LTScorrNorm)
    plt.title("LTS Cross-Correlation of Received Signal")
    plt.show()
    
    #Get CSI from the LTS
    firstLTS = sampsRecv[firstPeak:firstPeak+64]
    secondLTS = sampsRecv[firstPeak+64:firstPeak+128]
 
    yf1 = fftshift(fft(firstLTS,64)) #FFT of firstCSI
    yf2 = fftshift(fft(secondLTS,64)) #FFT of secondCSI
    CSI  = ((yf1+yf2)/2)*lts_freq #Average the FFTs and create channel estimates
    oneRep = yf1/CSI
    
    #Plot I/Q constellation
    plt.figure()
    plt.title("Constellation of 1 rep")
    plt.xlabel("In-Phase")
    plt.ylabel("Quadrature")
    plt.scatter(oneRep.real, oneRep.imag)
    plt.show() 
    
    #Plot Channel Estimates MAG
    plt.figure()
    fVals = np.arange(start = -64/2,stop = 64/2)*rate/64
    plt.plot(fVals, np.abs(CSI),'b')
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Amplitude")
    plt.title("Amplitude of Channel Estimates")
    plt.show()
    
    #Plot Channel Estimate ANG
    plt.figure()
    fVals = np.arange(start = -64/2,stop = 64/2)*rate/64
    plt.plot(fVals, np.angle(CSI),'b')
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Phase (Radians)")
    plt.title("Phase of Channel Estimates")
    plt.show()   
    
    #Get the OFDM Symbols following the LTS
    sampsRecv = sampsRecv[firstPeak+128:firstPeak+128+96*numOFDMSymbols] #Each OFDM Symbol is 96 symbols long (has 32 bit CP)
    
    #Take each OFDM symbol, remove the CP, demodulate and equalize
    for i in range(0,numOFDMSymbols):
        if i == 0:
            sampsRecvCAT = OFDM_Demodulate(sampsRecv[i*96:i*96+96])
        else:
            sampsRecvi = OFDM_Demodulate(sampsRecv[i*96:i*96+96])
            sampsRecvCAT = np.concatenate((sampsRecvCAT,sampsRecvi))
    #These are the data symbols
    sampsRecv = sampsRecvCAT
    
    # #FFT for ODFM
    # sampsRecv = np.fft.fft(sampsRecv)
    
    # #Retrieve the data Carrier Symbols
    # sampsRecv = sampsRecv[dataCarriers]
    
    # #Equalization
    # sampsRecv = sampsRecv/CSI[dataCarriers]
    
    #Plot I/Q constellation
    plt.figure()
    plt.title("Constellation of Received Signal")
    plt.xlabel("In-Phase")
    plt.ylabel("Quadrature")
    plt.scatter(sampsRecv.real, sampsRecv.imag)
    plt.show()
    
    #Demodulation
    demodData = np.zeros(len(sampsRecv))
    if modType == "DPSK":
        sampsRecv = np.concatenate((np.array([0.5+0.5j]),sampsRecv)) #Assume the first phase is A just like in transmission
        for i in range(1,len(sampsRecv)) :
            ang = np.angle(sampsRecv[i]) - np.angle(sampsRecv[i-1])
            if ang > np.pi:
                ang = ang -2*np.pi
            elif ang < -np.pi:
                ang = ang + 2*np.pi
            demodData[i-1] = DPSK_Demodulate(ang)
            #Transform symbols into bits
            demodBits = ''
            for a in demodData:
                demodBits = demodBits + format(int(a),'b').zfill(2)
    
    #Convert the bits back to text from ASCII
    # initializing a empty string for
    # storing the string data
    text =' '
      
    # slicing the input and converting it
    # in decimal and then converting it in string
    for i in range(0, len(demodBits), 8):
        # slicing the bin_data from index range [0, 6]
        # and storing it in temp_data
        temp_data = demodBits[i:i + 8]
          
        # Get decimal value of corresponding temp_data
        decimal_data = int(temp_data,2)
          
        # Decoding the decimal value returned by
        # BinarytoDecimal() function, using chr()
        # function which return the string corresponding
        # character for given ASCII value, and store it
        # in str_data
        text = text + chr(decimal_data)
    
    print("Received Text is: %s" %text)
    
    #BER Counter (We are asked to cheat since we know the data. In real-life
    #we would use integrity measures via a MAC to generate this, or a checksum)
    BER = 0
    for i in range(len(bitsBuffer)):
        if bitsBuffer[i] != demodBits[i]:
            BER += 1
    print("The BER = %d" %BER)
    
    # #Plot Auto-Correlation of STS
    # plt.figure()
    # plt.plot(STSaccum.real)
    # plt.title("STS Auto-Correlation of Received Signal")
    # plt.xlabel("Sample Number")
    # plt.ylabel("Value")
    # plt.show()
    
    #SoftShutdown
    print("Cleaning up streams")
    sdr.deactivateStream(rxStream)
    sdr.closeStream(rxStream)
    print("Done!")
    sys.exit(0)
    
else:
    print("Error, no mode selected")
    sys.exit(-1)

