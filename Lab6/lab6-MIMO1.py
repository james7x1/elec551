# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 16:56:07 2021

@author: jam37
This script is used to implement 2x2 MIMO in a virtual environment
"""

import SoapySDR
import scipy
import time
import sys
import signal
import os
import random
import numpy as np
from matplotlib import pyplot as plt
from scipy.fft import fft, fftfreq, fftshift
from scipy import io
from SoapySDR import * #SOAPY_SDR_constants

def genSTS(cp = 32):
    '''The genSTS function creates a time-domain 802.11 STS using the frequency values of the sequence and a cyclic prefix of "cp" (32). Modified from LTS.py'''
    signal = np.fft.ifft(np.fft.ifftshift(sts_freq))
    signal = signal/np.absolute(signal).max()*0.5  #normalize to 0.5
    signal = np.concatenate((signal[len(signal) - cp:], signal, signal)) #apply cyclic-prefix and repeat
    
    return signal    

def genLTS(upsample=1, cp=32):
    '''Generate a time-domain 802.11 LTS with a cyclic prefix of "cp" (32) and upsampled by a factor of "up" (1). Copied from LTS.py'''
    up_zeros = np.zeros(len(lts_freq)//2*(upsample-1))
    lts_freq_up = np.concatenate((up_zeros,lts_freq,up_zeros))
    signal = np.fft.ifft(np.fft.ifftshift(lts_freq_up))
    signal = signal/np.absolute(signal).max()*0.5  #normalize to 0.5
    
    #Now affix the cyclic prefix
    signal = np.concatenate((signal[len(signal) - cp:], signal, signal))  #could use tile...

    return signal

def modulate(data, modType, modOrder):
    '''The modulate function takes in the bytes of data, the type of modulation, and the order of modulation. It maps symbol of data to the correct complex value and passes it out as modulatedData '''
    #Create the modulation mapping array
    if modType == "QAM":
        if modOrder == 4:
            modMap = [-0.5+0.5j, -0.5-0.5j,  0.5+0.5j,  0.5-0.5j]
            #2 bits/symbol, so the shift is
            shift = 2
        if modOrder == 16:
            modMap = [-0.5       +0.5j       , -0.5       +0.16666667j,
        -0.5       -0.5j       , -0.5       -0.16666667j,
        -0.16666667+0.5j       , -0.16666667+0.16666667j,
        -0.16666667-0.5j       , -0.16666667-0.16666667j,
         0.5       +0.5j       ,  0.5       +0.16666667j,
         0.5       -0.5j       ,  0.5       -0.16666667j,
         0.16666667+0.5j       ,  0.16666667+0.16666667j,
         0.16666667-0.5j       ,  0.16666667-0.16666667j]
            #4 bits/symbol, so the shift is
            shift = 4
        if modOrder == 64:
            modMap = [-0.5       +0.5j       , -0.5       +0.35714286j,
        -0.5       +0.07142857j, -0.5       +0.21428571j,
        -0.5       -0.5j       , -0.5       -0.35714286j,
        -0.5       -0.07142857j, -0.5       -0.21428571j,
        -0.35714286+0.5j       , -0.35714286+0.35714286j,
        -0.35714286+0.07142857j, -0.35714286+0.21428571j,
        -0.35714286-0.5j       , -0.35714286-0.35714286j,
        -0.35714286-0.07142857j, -0.35714286-0.21428571j,
        -0.07142857+0.5j       , -0.07142857+0.35714286j,
        -0.07142857+0.07142857j, -0.07142857+0.21428571j,
        -0.07142857-0.5j       , -0.07142857-0.35714286j,
        -0.07142857-0.07142857j, -0.07142857-0.21428571j,
        -0.21428571+0.5j       , -0.21428571+0.35714286j,
        -0.21428571+0.07142857j, -0.21428571+0.21428571j,
        -0.21428571-0.5j       , -0.21428571-0.35714286j,
        -0.21428571-0.07142857j, -0.21428571-0.21428571j,
         0.5       +0.5j       ,  0.5       +0.35714286j,
         0.5       +0.07142857j,  0.5       +0.21428571j,
         0.5       -0.5j       ,  0.5       -0.35714286j,
         0.5       -0.07142857j,  0.5       -0.21428571j,
         0.35714286+0.5j       ,  0.35714286+0.35714286j,
         0.35714286+0.07142857j,  0.35714286+0.21428571j,
         0.35714286-0.5j       ,  0.35714286-0.35714286j,
         0.35714286-0.07142857j,  0.35714286-0.21428571j,
         0.07142857+0.5j       ,  0.07142857+0.35714286j,
         0.07142857+0.07142857j,  0.07142857+0.21428571j,
         0.07142857-0.5j       ,  0.07142857-0.35714286j,
         0.07142857-0.07142857j,  0.07142857-0.21428571j,
         0.21428571+0.5j       ,  0.21428571+0.35714286j,
         0.21428571+0.07142857j,  0.21428571+0.21428571j,
         0.21428571-0.5j       ,  0.21428571-0.35714286j,
         0.21428571-0.07142857j,  0.21428571-0.21428571j]
            #6 bits/symbol, so the shift is
            shift = 6
    if modType == "PSK":
        if modOrder == 8:
            modMap = [ 5.00000000e-01+0.00000000e+00j,  3.53553391e-01+3.53553391e-01j,
         3.06161700e-17+5.00000000e-01j, -3.53553391e-01+3.53553391e-01j,
        -5.00000000e-01+6.12323400e-17j, -3.53553391e-01-3.53553391e-01j,
        -9.18485099e-17-5.00000000e-01j,  3.53553391e-01-3.53553391e-01j]
            #3 bits/symbol
            shift = 3
        if modOrder == 16:
            modMap = [ 5.00000000e-01+0.00000000e+00j,  4.61939766e-01+1.91341716e-01j,
         3.53553391e-01+3.53553391e-01j,  1.91341716e-01+4.61939766e-01j,
         3.06161700e-17+5.00000000e-01j, -1.91341716e-01+4.61939766e-01j,
        -3.53553391e-01+3.53553391e-01j, -4.61939766e-01+1.91341716e-01j,
        -5.00000000e-01+6.12323400e-17j, -4.61939766e-01-1.91341716e-01j,
        -3.53553391e-01-3.53553391e-01j, -1.91341716e-01-4.61939766e-01j,
        -9.18485099e-17-5.00000000e-01j,  1.91341716e-01-4.61939766e-01j,
         3.53553391e-01-3.53553391e-01j,  4.61939766e-01-1.91341716e-01j]
            #4 bits/symbol
            shift = 4
    if modType == "DPSK":
        if modOrder == 4:
            #|C    A|
            #|D    B|
            #00 = 0
            #01 = +pi/2
            #10 = -pi/2
            #11 = +pi
            #2 bits/symbol, so the shift is
            shift = 2
    #Now break up the data in symbols sizes and map it to the symbol values
    numberOfSymbols = int(np.ceil(len(data)/shift))
    modulatedData = np.zeros(numberOfSymbols,np.complex64)
    for symbolNum in range(0,numberOfSymbols) :
        if modType == "DPSK":
            if symbolNum == 0:
                #Assume A is the first arbitary symbol sent
                modIndex = int(data[symbolNum*shift:symbolNum*shift+shift].ljust(shift,'0'),2)
                modulatedData[symbolNum] = DPSK_0Switch(modIndex)
            else:    
                modIndex = int(data[symbolNum*shift:symbolNum*shift+shift].ljust(shift,'0'),2)
                modulatedData[symbolNum] = DPSK_Switch(modIndex,modulatedData[symbolNum-1])
        else:
            modIndex = int(data[symbolNum*shift:symbolNum*shift+shift].ljust(shift,'0'),2)
            modulatedData[symbolNum] = modMap[modIndex];
        
    if modType == "DPSK":
        modulatedData = np.concatenate((np.array([0.5+0.5j]),modulatedData))
    return modulatedData

def upSample(modulated):
    upSampledSig = np.array([],dtype=np.complex64)
    for samp in range(0,len(modulated)) :
        pulse = np.zeros(resampleFactor,dtype=np.complex64)
        pulse[0] = modulated[samp]
        upSampledSig = np.concatenate((upSampledSig, pulse))
    return upSampledSig

def DPSK_0Switch(argument):
    switcher = {
        0: np.array([0.5+0.5j]),
        1: np.array([-0.5+0.5j]),
        2: np.array([0.5-0.5j]),
        3: np.array([-0.5-0.5j])
    }
    return switcher.get(argument, "Invalid symbol")

def DPSK_Switch(argument,lastSymbol):
    switcher = {
        0: lastSymbol,
        1: lastSymbol*np.exp(1j*np.pi/2),
        2: lastSymbol*np.exp(1j*-np.pi/2),
        3: lastSymbol*np.exp(1j*np.pi)
    }
    return switcher.get(argument, "Invalid symbol")

def LTSCrossCorr(sampsRecv):
    #Cross-Correlation for LTS
    LTScorr = np.correlate(sampsRecv,genLTS()[32:64+32],'full')
    LTScorrNorm = np.abs(LTScorr.real)/np.abs(LTScorr.real).max() #Normalize correlation
    
    #Plot Cross-Correlation of LTS
    plt.figure()
    plt.plot(LTScorrNorm)
    plt.title("LTS Cross-Correlation of Received Signal")
    plt.show()
    
    #Find index of the first peak and correct for zero padding to get signal index of the start of LTS
    #Better way is to use different gold codes or cyclicly offset the 2nd LTS.
    peaks = np.nonzero(LTScorrNorm>=LTSThresh)[0] #index of peaks in correlation
    for peak in peaks:
        foundFirstLTSRep = peak+64 in peaks
        #foundFirstTrans = peak+128+32 in peaks #64 length LTS+training symbols + 32 length CP
        if (foundFirstLTSRep): # and foundFirstTrans):
            firstPeak = peak-64 #index of beginning of LTS in signal
            break;

    CFOaccum = 0
    # for i in range(0,64):
    #     CFOaccum = CFOaccum + (np.angle(sig[firstPeak+64+i])-np.angle(sig[firstPeak+i]))
    # CFOavg = CFOaccum/64
    CFO = (np.angle(sampsRecv[firstPeak+64])-np.angle(sampsRecv[firstPeak]))
    CFO_out = CFO*rate/(2*np.pi*64)
    print("The CFO = %f" % CFO_out)
    #Correct for sample accurate timing by making the beginning of the LTS the start of the buffer through truncation
    LTScorrNorm = LTScorrNorm[firstPeak+64:]
    
    return firstPeak

def DPSK_Demodulate(angle):
    if -np.pi/4<angle<np.pi/4:
        return 0
    if np.pi/4<angle<3*np.pi/4:
        return 1
    if -3*np.pi/4<angle<-np.pi/4:
        return 2
    if 3*np.pi/4<angle<np.pi or -np.pi<angle<-3*np.pi/4:
        return 3

def softShutdown(signum, frame):
    print("Cleaning up streams")
    sdr.deactivateStream(txStream)
    sdr.closeStream(txStream)
    print("Done!")
    sys.exit(0)    

#Checking Arguments
if len(sys.argv) != 6:
        print("Usage:python %s IrisSerial, tx/rx, gain, modulation type, order" % os.path.basename(__file__))
        sys.exit(-1)

#Now for the parameters of the CW transmission
serial = sys.argv[1]
mode = sys.argv[2] #tx/rx
gain = int(sys.argv[3]) #The amplitude gain for the reciever
modType = sys.argv[4] #QAM, PSK, DPSK
modOrder = int(sys.argv[5]) #4, 8, 16, 64
chan = [0,1]
rate = 5e6 #Digital Sampling Frequency
freq = 2.41e9 #RF Frequency of 2.49 GHz
delay = int(30e6) #Tx hardware setup delay in ns
alpha = 0.9564 #In-phase IQ imbalance coefficient
beta = 1.0 #Quadrature IQ imbalance coefficient
resampleFactor = 8 #Upsample tx side, Downsample rx side
digitalIF = rate #Digital IF for Up/DownConversion
digitalRate = rate*resampleFactor #New digital sampling rate DISABLE if not upsampling!
sts_freq = np.array([0,0,0,0,0,0,0,0,1+1j,0,0,0,-1-1j,0,0,0,1+1j,0,0,0,-1-1j,0,0,0,-1-1j,0,0,0,1+1j,0,0,0,0,0,0,0,-1-1j,0,0,0,-1-
1j,0,0,0,1+1j,0,0,0,1+1j,0,0,0,1+1j,0,0,0,1+1j,0,0,0,0,0,0,0]) #copied from lecture notes
lts_freq = np.array([0,0,0,0,0,0,1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,1,1,-1,-1,1,1,-1,1,-1,1,1,1,1,0,1,-1,-1,1,1,-1,1,-1,1,-1,-1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,1,1,1,1,0,0,0,0,0]) #copied from lts.py
LTSThresh = 0.8

bitsBuffer0 = ''.join(format(i, '08b') for i in bytearray('Hello, my name is Allen!!! Where is my IEEE Merch???', encoding ='utf-8'))
bitsBuffer1 = ''.join(format(i, '08b') for i in bytearray('I miss my girlfriend...Too bad she is not a radio!:)', encoding ='utf-8'))
lenUpConvertedSig = 1736 #Get this number by running the tx code :)

#Create Training Symbols
trainingBits = ''.join(format(i, '08b') for i in bytearray('?', encoding ='utf-8'))

#Setup sequence of bits (they need to be upconverted together to prevent spikes)
#sig1: Training   |0's        |Data
#sig2: 0's        |Training   |Data
bitsBuffer0 = trainingBits+'00000000'+bitsBuffer0                
bitsBuffer1 = '00000000'+trainingBits+bitsBuffer1

if mode == "tx":
    #Modulate signal
    modulatedSig0 = modulate(bitsBuffer0,modType,modOrder)
    modulatedSig1 = modulate(bitsBuffer1,modType,modOrder)
    
    #Upsample signal
    upSampledSig0 = upSample(modulatedSig0)
    upSampledSig1 = upSample(modulatedSig1)
    digitalRate = rate*resampleFactor #New digital sampling rate    
    
    #Filter the Signal
    #rrcos
    filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/elec551-main/Lab3/rrcos01-post.mat')['tfrrcos']
    filterOut0 = np.convolve(upSampledSig0,filterTF[0,:])
    filterOut0 = filterOut0[int(np.floor(len(filterTF[0,:])/2)):int(-1*np.floor(len(filterTF[0,:])/2))]
    
    filterOut1 = np.convolve(upSampledSig1,filterTF[0,:])
    filterOut1 = filterOut1[int(np.floor(len(filterTF[0,:])/2)):int(-1*np.floor(len(filterTF[0,:])/2))]
    
    #UpConvert signal
    period = (1/digitalIF)*digitalRate;
    stime = np.linspace(0,1/digitalIF,len(filterOut0))
    upConvertSinusoid = np.exp(1j*2*np.pi*digitalIF*stime*len(filterOut0)/period)
    upConvertedSig0 = filterOut0*upConvertSinusoid
    upConvertedSig1 = filterOut1*upConvertSinusoid
    print("len(upconvertedSig) = %d" % (len(upConvertedSig0)))
    
    #Declare signals to transmit
    #sig1: LTS|Training   |0's        |Data|0's #8 zeros at end for noise estimation
    #sig2: 0's|0's        |Training   |Data|0's
    sig1 = np.concatenate((genLTS(),upConvertedSig0,np.zeros(8))) #Chan 0
    #sig1 = np.concatenate((genLTS(),np.zeros(len(upConvertedSig0)),np.zeros(8))) #Chan 0
    sig2 = np.concatenate((np.zeros(len(genLTS())),upConvertedSig1,np.zeros(8))) #Chan 1
    #sig2 = np.concatenate((np.zeros(len(genLTS())),np.zeros(len(upConvertedSig1)),np.zeros(8))) #Chan 1
    nsamps = len(sig1)
    print(nsamps)
    
    #DEBUGGING: Plotting for Transmitted Signal
    #Plot Time-Domain
    #Plot Time-Domain
    plt.figure()
    plt.title('Time-Domain of Transmitted Signal')
    plt.xlabel('Sample Number')
    plt.ylabel('Sample Value')
    plt.plot((sig1+sig2).real,label = "Real")
    plt.plot((sig1+sig2).imag,label = "Imaginary")
    plt.legend(loc="upper right")
    plt.show() 
    
    #Plot Constellation
    plt.figure()
    plt.scatter((sig1+sig2).real,(sig1+sig2).imag)
    plt.title("Transmitted Signal Constellation")
    plt.xlabel("In-Phase")
    plt.ylabel('Quadrature')
    plt.show()
    
    #Plot PSD
    plt.figure()
    plt.title('PSD of Transmitted Signal')
    plt.psd((sig1+sig2),len(sig1),rate)
    plt.show()
    
    
    #Init Tx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    for chan in [0,1]:
        sdr.setSampleRate(SOAPY_SDR_TX, chan, rate)
        sdr.setFrequency(SOAPY_SDR_TX, chan, "RF", freq)
        sdr.setAntenna(SOAPY_SDR_TX, chan, "TRX")
        sdr.setGain(SOAPY_SDR_TX, chan, gain)
        
    
    #Init Tx Streams
    txStream = sdr.setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32, [0,1], {})
    ts = sdr.getHardwareTime() + delay #give us delay in nanosec to set everything up.
    txFlags = SOAPY_SDR_HAS_TIME
    sdr.activateStream(txStream)
    
    t=time.time()
    total_samps = 0
    
    #First Transmission needs a time stamp.
    sr = sdr.writeStream(txStream, [sig1.astype(np.complex64),sig2.astype(np.complex64)], len(sig1), txFlags, ts+delay)
    if sr.ret != len(sig1):
            print("Bad Write!!!")
            
    #Catch CTRL-C
    signal.signal(signal.SIGINT, softShutdown)
            
    #continuously transmit until the script is killed
    while True :
        sr = sdr.writeStream(txStream, [sig1.astype(np.complex64),sig2.astype(np.complex64)], len(sig1), 0)
        if sr.ret != len(sig1):
            print("Bad Write!!!")
        
        #print an update
        total_samps += len(sig1)
        if time.time() - t > 1:
            t=time.time()
            print("Total Samples Sent: %i" % total_samps)

elif mode == "rx":
    nsamps = 8192 #Number of samples
    # Init Rx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    for chan in [0,1]:
        sdr.setSampleRate(SOAPY_SDR_RX, chan, rate)
        sdr.setFrequency(SOAPY_SDR_RX, chan, "RF", freq)
        sdr.setGain(SOAPY_SDR_RX, chan, gain)
        sdr.setAntenna(SOAPY_SDR_RX, chan, "RX")
        sdr.setDCOffsetMode(SOAPY_SDR_RX, chan, True) #dc removal on rx
    
    #Init buffers
    sampsRecv0 = np.zeros(nsamps, dtype=np.complex64)
    sampsRecv1 = np.zeros(nsamps, dtype=np.complex64)
    
    #Init Streams
    rxStream = sdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, [0,1], {})
    ts = sdr.getHardwareTime() + delay
    sdr.activateStream(rxStream, SOAPY_SDR_HAS_TIME|SOAPY_SDR_END_BURST, ts)
    
    t=time.time()
    total_samps = 0
    
    #Receive one packet
    sr = sdr.readStream(rxStream, [sampsRecv0,sampsRecv1], nsamps, timeoutUs= delay)    
    if sr.ret != nsamps:
        print("Bad Read!!!")
    
    #print an update
    total_samps += nsamps
    print("Total Samples Received: %i" % total_samps)
    #do some rx processing here
    
    # #IQ Imbalance Correction
    # sampsRecv = alpha*np.real(sampsRecv) + beta*1j*np.imag(sampsRecv)
    
    # #Auto-Correlation for STS
    # delayed = np.concatenate((np.complex128(np.zeros(16)),sampsRecv))
    # sampsRecv_padded = np.concatenate((sampsRecv,np.complex128(np.zeros(16))))
    # conj_d = np.conj(delayed)
    # multDelay = sampsRecv_padded*conj_d
    # STSaccum = np.zeros(len(multDelay))
    # for i in range(0,len(multDelay)):
    #     STSaccum[i]=np.sum(multDelay[0:i])
        
    #Plot Time-Domain of Signal
    plt.figure()
    plt.plot(sampsRecv0.real,label = "Real chan0")
    plt.plot(sampsRecv1.real ,label = "Real chan1")
    plt.title("Time Domain of Received Signal")
    plt.legend(loc="upper right")
    plt.xlabel("Sample")
    plt.ylabel("Amplitude")
    plt.show()
    
    #Plot FFT
    plt.figure()
    plt.title("FFT of Received Signal")
    fVals = np.arange(start = -nsamps/2,stop = nsamps/2)*rate/nsamps
    yf = fftshift(fft(sampsRecv0,nsamps))
    plt.plot(fVals,np.abs(yf),'b', label = "chan0")
    yf = fftshift(fft(sampsRecv1,nsamps))
    plt.plot(fVals,np.abs(yf),'b', label = "chan1")
    plt.show()
    
    #Plot PSD
    plt.figure()
    plt.title("PSD of Chan0")
    plt.xlabel("Frequency of Received Signal")
    plt.ylabel("Power in dBm")
    [Pxx, freqs] = plt.psd(sampsRecv0,len(sampsRecv0),rate,0)
    plt.show()
    
    #Find the beginning of the signal (Looking for the LTS of the first antenna)
    firstPeak0 = LTSCrossCorr(sampsRecv0)
    firstPeak1 = LTSCrossCorr(sampsRecv1)
    #Remove LTS
    sampsRecv0 = sampsRecv0[firstPeak0+128:firstPeak0+128+lenUpConvertedSig]
    sampsRecv1 = sampsRecv1[firstPeak1+128:firstPeak1+128+lenUpConvertedSig]
        
    #Downconvert samples
    period = int((1/digitalIF)*digitalRate);
    stime = np.linspace(0,1/digitalIF,len(sampsRecv0))
    downConvertSinusoid = np.exp(-1j*2*np.pi*digitalIF*stime*len(sampsRecv0)/period)
    sampsRecv0 = sampsRecv0*downConvertSinusoid
    sampsRecv1 = sampsRecv1*downConvertSinusoid
    
    #Filter
    #rrcos
    filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/elec551-main/Lab3/rrcos01-post.mat')['tfrrcos']
    sampsRecv0 = np.convolve(sampsRecv0,filterTF[0,:])
    sampsRecv1 = np.convolve(sampsRecv1,filterTF[0,:])

    #Downsample samples
    sampsRecv0 = sampsRecv0[1::resampleFactor]
    sampsRecv1 = sampsRecv1[1::resampleFactor]
    
    #Remove effects of filter
    sampsRecv0 = sampsRecv0[16:-16]
    sampsRecv1 = sampsRecv1[16:-16]

    #Channel Estimation
    #Training Symbols
    train = modulate(trainingBits,modType,modOrder)
    #Get Training Symbols
    h11 = np.average(sampsRecv0[0:5]/train)
    h12 = np.average(sampsRecv1[0:5]/train)
    h21 = np.average(sampsRecv0[5:5+5]/train)
    h22 = np.average(sampsRecv1[5:5+5]/train)
    H = np.array([[h11,h12], [h21,h22]])
    
    #Beamweights
    Wrx = np.linalg.pinv(H)
    #Make buffers into a column vector
    y = np.array(([sampsRecv0],[sampsRecv1]))
    # yprime = np.matmul(Wrx,y)
    # sampsRecv0 = yprime[0][0]
    # sampsRecv1 = yprime[1][0]
    #Manually doing the matrix multiplication since they are buffers
    sampsRecv0 = Wrx[0][0]*y[0][0] + Wrx[0][1]*y[1][0]
    sampsRecv1 = Wrx[1][0]*y[0][0] + Wrx[1][1]*y[1][0]

    # #Remove Training Symbols
    # sampsRecv0 = sampsRecv0[10::]
    # sampsRecv1 = sampsRecv1[10::]

    #Plot I/Q constellation
    plt.figure()
    plt.title("Constellation of Received Signal")
    plt.xlabel("In-Phase")
    plt.ylabel("Quadrature")
    plt.scatter(sampsRecv0.real, sampsRecv0.imag)
    plt.show()
    
    #Plot I/Q constellation
    plt.figure()
    plt.title("Constellation of Received Signal")
    plt.xlabel("In-Phase")
    plt.ylabel("Quadrature")
    plt.scatter(sampsRecv1.real, sampsRecv1.imag)
    plt.show()    
    
    #Demodulation
    demodData0 = np.zeros(len(sampsRecv0))
    if modType == "DPSK":
        # sampsRecv = np.concatenate((np.array([0.5+0.5j]),sampsRecv)) #Assume the first phase is A just like in transmission
        for i in range(1,len(sampsRecv0)) :
            ang = np.angle(sampsRecv0[i]) - np.angle(sampsRecv0[i-1])
            if ang > np.pi:
                ang = ang -2*np.pi
            elif ang < -np.pi:
                ang = ang + 2*np.pi
            demodData0[i-1] = DPSK_Demodulate(ang)
            #Transform symbols into bits
            demodBits0 = ''
            for a in demodData0:
                demodBits0 = demodBits0 + format(int(a),'b').zfill(2)
                
    demodData1 = np.zeros(len(sampsRecv1))
    if modType == "DPSK":
        # sampsRecv = np.concatenate((np.array([0.5+0.5j]),sampsRecv)) #Assume the first phase is A just like in transmission
        for i in range(1,len(sampsRecv1)) :
            ang = np.angle(sampsRecv1[i]) - np.angle(sampsRecv1[i-1])
            if ang > np.pi:
                ang = ang -2*np.pi
            elif ang < -np.pi:
                ang = ang + 2*np.pi
            demodData1[i-1] = DPSK_Demodulate(ang)
            #Transform symbols into bits
            demodBits1 = ''
            for a in demodData1:
                demodBits1 = demodBits1 + format(int(a),'b').zfill(2)
    
    #Convert the bits back to text from ASCII
    # initializing a empty string for
    # storing the string data
    text0 =' '
    text1 =' '
      
    # slicing the input and converting it
    # in decimal and then converting it in string
    for i in range(0, len(demodBits0), 8):
        # slicing the bin_data from index range [0, 6]
        # and storing it in temp_data
        temp_data = demodBits0[i:i + 8]
          
        # Get decimal value of corresponding temp_data
        decimal_data = int(temp_data,2)
          
        # Decoding the decimal value returned by
        # BinarytoDecimal() function, using chr()
        # function which return the string corresponding
        # character for given ASCII value, and store it
        # in str_data
        text0 = text0 + chr(decimal_data)
    
    print("Received Text is: %s" %text0)
    #BER Counter (We are asked to cheat since we know the data. In real-life
    #we would use integrity measures via a MAC to generate this, or a checksum)
    BER0 = 0
    for i in range(len(bitsBuffer0)):
        if bitsBuffer0[i] != demodBits0[i]:
            BER0 += 1
    print("The BER = %d" %BER0)
    
    for i in range(0, len(demodBits1), 8):
        # slicing the bin_data from index range [0, 6]
        # and storing it in temp_data
        temp_data = demodBits1[i:i + 8]
          
        # Get decimal value of corresponding temp_data
        decimal_data = int(temp_data,2)
          
        # Decoding the decimal value returned by
        # BinarytoDecimal() function, using chr()
        # function which return the string corresponding
        # character for given ASCII value, and store it
        # in str_data
        text1 = text1 + chr(decimal_data)
    print("Received Text is: %s" %text1) 
    BER1 = 0
    #Only look at data
    # bitsBuffer1 = bitsBuffer[16::]
    for i in range(len(bitsBuffer1)):
        if bitsBuffer1[i] != demodBits1[i]:
            BER1 += 1
    print("The BER = %d" %BER1)    

    #SoftShutdown
    print("Cleaning up streams")
    sdr.deactivateStream(rxStream)
    sdr.closeStream(rxStream)
    print("Done!")
    sys.exit(0)    
else:
    print("Error, no mode selected")
    sys.exit(-1)