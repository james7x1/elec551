# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 16:56:07 2021

@author: jam37
This script is used to transmit or receive a CW transmission.
python lab1b.py serial tx/rx gain
"""

import SoapySDR
import time
import sys
import os
import numpy as np
from matplotlib import pyplot as plt
from scipy.fft import fft, fftfreq, fftshift
from SoapySDR import * #SOAPY_SDR_constants

#Checking Arguments
if len(sys.argv) != 4:
		print("Usage:python %s IrisSerial tx/rx gain" % os.path.basename(__file__))
		sys.exit(-1)

#Now for the parameters of the CW transmission
serial = sys.argv[1]
chan = 0
rate = 10e6 #10 Msps
freq = 2.451e9 #RF Frequency of 2.49 GHz
gain = int(sys.argv[3]) #The amplitude gain for the reciever
delay = int(30e6) #Tx hardware setup delay in ns
mode = sys.argv[2] #tx/rx
alpha = 0.9564 #In-phase IQ imbalance coefficient
beta = 1.0 #Quadrature IQ imbalance coefficient

if mode == "tx":
    #Init Signal
    BBfreq = 500e3 # Baseband Frequency of sinusoid
    stime = np.linspace(0,1/BBfreq,int((1/BBfreq)*rate))
    sig1 = (1.0/np.sqrt(2.0))*np.exp(1j*2.0*np.pi*BBfreq*stime)
    sig = np.tile(sig1,100)
    nsamps = len(sig)
    
    #DEBUGGING: Plotting for Transmitted Signal
    # plt.figure()
    # plt.plot(np.real(sig))
    # plt.plot(np.imag(sig))
    # plt.show()
    
    # plt.figure()
    # fVals = np.arange(start = -nsamps/2,stop = nsamps/2)*rate/nsamps
    # yf = fftshift(fft(sig))
    # plt.plot(fVals,np.abs(yf),'b')
    # plt.show()
    
    #Init Tx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    sdr.setSampleRate(SOAPY_SDR_TX, chan, rate)
    sdr.setFrequency(SOAPY_SDR_TX, chan, "RF", freq)
    sdr.setGain(SOAPY_SDR_TX, chan, gain)
    sdr.setAntenna(SOAPY_SDR_TX, chan, "TRX")
    
    #Init Tx Streams
    txStream = sdr.setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32, [0], {})
    ts = sdr.getHardwareTime() + delay #give us delay in nanosec to set everything up.
    txFlags = SOAPY_SDR_HAS_TIME
    sdr.activateStream(txStream)
    
    t=time.time()
    total_samps = 0
    
    #First Transmission needs a time stamp.
    sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), txFlags, ts+delay)
    if sr.ret != len(sig):
            print("Bad Write!!!")
    #continuously transmit until the script is killed
    while True :
        sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), 0)
        if sr.ret != len(sig):
            print("Bad Write!!!")
        
        #print an update
        total_samps += len(sig)
        if time.time() - t > 1:
            t=time.time()
            print("Total Samples Sent: %i" % total_samps)
elif mode == "rx":
    nsamps = 8092 #Number of samples
    # Init Rx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    sdr.setSampleRate(SOAPY_SDR_RX, 0, rate)
    sdr.setFrequency(SOAPY_SDR_RX, 0, "RF", freq)
    sdr.setGain(SOAPY_SDR_RX, 0, gain)
    sdr.setAntenna(SOAPY_SDR_RX, 0, "RX")
    
    #Init buffers
    sampsRecv = np.zeros(nsamps, dtype=np.complex64)
    
    #Init Streams
    rxStream = sdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, [0], {})
    ts = sdr.getHardwareTime() + delay
    sdr.activateStream(rxStream, SOAPY_SDR_HAS_TIME, ts)
    
    t=time.time()
    total_samps = 0
    
    #Receive one packet
    sr = sdr.readStream(rxStream, [sampsRecv], nsamps, timeoutUs= delay)    
    if sr.ret != nsamps:
    	print("Bad Read!!!")
    
    #print an update
    total_samps += nsamps
    print("Total Samples Received: %i" % total_samps)
    #do some rx processing here
    
    #DC Offset Correction
    sampsRecv = (np.real(sampsRecv)-np.average(np.real(sampsRecv))) + 1j*(np.imag(sampsRecv)-np.average(np.imag(sampsRecv)))
    
    #IQ Imbalance Correction
    sampsRecv = alpha*np.real(sampsRecv) + beta*1j*np.imag(sampsRecv)
    
    #Plot I/Q in time domain
    plt.figure()
    plt.plot(np.real(sampsRecv))
    plt.plot(np.imag(sampsRecv))
    plt.show()
    
    #Plot I/Q constellation
    plt.figure()
    plt.title("I/Q After DC and IQ Imbalance Correction")
    plt.xlabel("In-Phase")
    plt.ylabel("Quadrature")
    plt.scatter(np.real(sampsRecv), np.imag(sampsRecv))
    plt.show()
    
    #Plot FFT
    plt.figure()
    fVals = np.arange(start = -nsamps/2,stop = nsamps/2)*rate/nsamps
    yf = fftshift(fft(sampsRecv,nsamps))
    plt.plot(fVals,np.abs(yf),'b')
    plt.show()
    
    #Plot PSD
    plt.figure()
    plt.title("PSD After DC and IQ Imbalance Correction")
    plt.xlabel("Frequency")
    plt.ylabel("Power in dBm")
    [Pxx, freqs] = plt.psd(sampsRecv,nsamps,rate,0)
    plt.show()
    
else:
    print("Error, no mode selected")
    sys.exit(-1)