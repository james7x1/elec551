# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 16:56:07 2021

@author: jam37
This script is used to create a CW transmission.
"""

import SoapySDR
import time
import numpy as np
from matplotlib import pyplot as plt
from scipy.fft import fft, fftfreq, fftshift
from SoapySDR import * #SOAPY_SDR_constants
#Now for the parameters of the CW transmission
serial = "VG2B000004"
chan = 0
rate = 10e6 #10 Msps
freq = 2.451e9 #RF Frequency of 2.49 GHz
txGain = 50 #The amplitude gain for the reciever
delay = int(30e6) #Tx hardware setup delay in ns

#Init Signal
BBfreq = 500e3 # Baseband Frequency of sinusoid
stime = np.linspace(0,1/BBfreq,int((1/BBfreq)*rate))
sig1 = (1.0/np.sqrt(2.0))*np.exp(1j*2.0*np.pi*BBfreq*stime)
sig = np.tile(sig1,100)
nsamps = len(sig)

# plt.figure()
# plt.plot(np.real(sig))
# plt.plot(np.imag(sig))
# plt.show()

# plt.figure()
# fVals = np.arange(start = -nsamps/2,stop = nsamps/2)*rate/nsamps
# yf = fftshift(fft(sig))
# plt.plot(fVals,np.abs(yf),'b')
# plt.show()

#Init Tx
sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
sdr.setSampleRate(SOAPY_SDR_TX, chan, rate)
sdr.setFrequency(SOAPY_SDR_TX, chan, "RF", freq)
sdr.setGain(SOAPY_SDR_TX, chan, txGain)
sdr.setAntenna(SOAPY_SDR_TX, chan, "TRX")

#Init Tx Streams
txStream = sdr.setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32, [0], {})
ts = sdr.getHardwareTime() + delay #give us delay in nanosec to set everything up.
txFlags = SOAPY_SDR_HAS_TIME
sdr.activateStream(txStream)

t=time.time()
total_samps = 0

#First Transmission needs a time stamp.
sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), txFlags, ts+delay)
if sr.ret != len(sig):
        print("Bad Write!!!")
#continuously transmit until the script is killed
while True :
    sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), 0)
    if sr.ret != len(sig):
        print("Bad Write!!!")
    
    #print an update
    total_samps += len(sig)
    if time.time() - t > 1:
        t=time.time()
        print("Total Samples Sent: %i" % total_samps)