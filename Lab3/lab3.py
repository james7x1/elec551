# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 16:56:07 2021

@author: jam37
This script is used to transmit or receive a modulated signal. Added
functionality to deal with filtering.
"""

import SoapySDR
import scipy
import time
import sys
import os
import random
import numpy as np
from matplotlib import pyplot as plt
from scipy.fft import fft, fftfreq, fftshift
from scipy import io
from SoapySDR import * #SOAPY_SDR_constants

#The modulate function takes in the bytes of data, the type of modulation, and
#the order of modulation. It maps symbol of data to the correct complex value
#and passes it out as modulatedData    
def modulate(data, modType, modOrder):
    #Create the modulation mapping array
    if modType == "QAM":
        if modOrder == 4:
            modMap = [-0.5+0.5j, -0.5-0.5j,  0.5+0.5j,  0.5-0.5j]
            #2 bits/symbol, so the shift is
            shift = 2
        if modOrder == 16:
            modMap = [-0.5       +0.5j       , -0.5       +0.16666667j,
        -0.5       -0.5j       , -0.5       -0.16666667j,
        -0.16666667+0.5j       , -0.16666667+0.16666667j,
        -0.16666667-0.5j       , -0.16666667-0.16666667j,
         0.5       +0.5j       ,  0.5       +0.16666667j,
         0.5       -0.5j       ,  0.5       -0.16666667j,
         0.16666667+0.5j       ,  0.16666667+0.16666667j,
         0.16666667-0.5j       ,  0.16666667-0.16666667j]
            #4 bits/symbol, so the shift is
            shift = 4
        if modOrder == 64:
            modMap = [-0.5       +0.5j       , -0.5       +0.35714286j,
        -0.5       +0.07142857j, -0.5       +0.21428571j,
        -0.5       -0.5j       , -0.5       -0.35714286j,
        -0.5       -0.07142857j, -0.5       -0.21428571j,
        -0.35714286+0.5j       , -0.35714286+0.35714286j,
        -0.35714286+0.07142857j, -0.35714286+0.21428571j,
        -0.35714286-0.5j       , -0.35714286-0.35714286j,
        -0.35714286-0.07142857j, -0.35714286-0.21428571j,
        -0.07142857+0.5j       , -0.07142857+0.35714286j,
        -0.07142857+0.07142857j, -0.07142857+0.21428571j,
        -0.07142857-0.5j       , -0.07142857-0.35714286j,
        -0.07142857-0.07142857j, -0.07142857-0.21428571j,
        -0.21428571+0.5j       , -0.21428571+0.35714286j,
        -0.21428571+0.07142857j, -0.21428571+0.21428571j,
        -0.21428571-0.5j       , -0.21428571-0.35714286j,
        -0.21428571-0.07142857j, -0.21428571-0.21428571j,
         0.5       +0.5j       ,  0.5       +0.35714286j,
         0.5       +0.07142857j,  0.5       +0.21428571j,
         0.5       -0.5j       ,  0.5       -0.35714286j,
         0.5       -0.07142857j,  0.5       -0.21428571j,
         0.35714286+0.5j       ,  0.35714286+0.35714286j,
         0.35714286+0.07142857j,  0.35714286+0.21428571j,
         0.35714286-0.5j       ,  0.35714286-0.35714286j,
         0.35714286-0.07142857j,  0.35714286-0.21428571j,
         0.07142857+0.5j       ,  0.07142857+0.35714286j,
         0.07142857+0.07142857j,  0.07142857+0.21428571j,
         0.07142857-0.5j       ,  0.07142857-0.35714286j,
         0.07142857-0.07142857j,  0.07142857-0.21428571j,
         0.21428571+0.5j       ,  0.21428571+0.35714286j,
         0.21428571+0.07142857j,  0.21428571+0.21428571j,
         0.21428571-0.5j       ,  0.21428571-0.35714286j,
         0.21428571-0.07142857j,  0.21428571-0.21428571j]
            #6 bits/symbol, so the shift is
            shift = 6
    if modType == "PSK":
        if modOrder == 8:
            modMap = [ 5.00000000e-01+0.00000000e+00j,  3.53553391e-01+3.53553391e-01j,
         3.06161700e-17+5.00000000e-01j, -3.53553391e-01+3.53553391e-01j,
        -5.00000000e-01+6.12323400e-17j, -3.53553391e-01-3.53553391e-01j,
        -9.18485099e-17-5.00000000e-01j,  3.53553391e-01-3.53553391e-01j]
            #3 bits/symbol
            shift = 3
        if modOrder == 16:
            modMap = [ 5.00000000e-01+0.00000000e+00j,  4.61939766e-01+1.91341716e-01j,
         3.53553391e-01+3.53553391e-01j,  1.91341716e-01+4.61939766e-01j,
         3.06161700e-17+5.00000000e-01j, -1.91341716e-01+4.61939766e-01j,
        -3.53553391e-01+3.53553391e-01j, -4.61939766e-01+1.91341716e-01j,
        -5.00000000e-01+6.12323400e-17j, -4.61939766e-01-1.91341716e-01j,
        -3.53553391e-01-3.53553391e-01j, -1.91341716e-01-4.61939766e-01j,
        -9.18485099e-17-5.00000000e-01j,  1.91341716e-01-4.61939766e-01j,
         3.53553391e-01-3.53553391e-01j,  4.61939766e-01-1.91341716e-01j]
            #4 bits/symbol
            shift = 4
    
    #Now break up the data in symbols sizes and map it to the symbol values
    #For every 8 byte of data
    #for bytenum in range(0,len(str(hex(bitsBuffer)[2::]))/2) :
    numberOfSymbols = int(len(str(bin(bitsBuffer)[2::]))/shift)
    modulatedData = np.zeros(numberOfSymbols,np.complex64)
    for symbolNum in range(0,int(len(str(bin(bitsBuffer)[2::]))/shift)) : #needs zero padding
        modIndex = int('0b'+str(bin(bitsBuffer))[2+symbolNum*shift:2+symbolNum*shift+shift],2)
        modulatedData[symbolNum] = modMap[modIndex]
        
    return modulatedData

#Checking Arguments
if len(sys.argv) != 6:
		print("Usage:python %s IrisSerial, tx/rx, gain, modulation type, order" % os.path.basename(__file__))
		sys.exit(-1)

#Now for the parameters of the CW transmission
serial = sys.argv[1]
mode = sys.argv[2] #tx/rx
gain = int(sys.argv[3]) #The amplitude gain for the reciever
modType = sys.argv[4] #QAM, PSK
modOrder = int(sys.argv[5]) #4, 8, 16, 64 
chan = 0
rate = 5e6 #Digital Sampling Frequency
freq = 2.451e9 #RF Frequency of 2.49 GHz
delay = int(30e6) #Tx hardware setup delay in ns
alpha = 0.9564 #In-phase IQ imbalance coefficient
beta = 1.0 #Quadrature IQ imbalance coefficient
resampleFactor = 8 #Upsample tx side, Downsample rx side
digitalIF = (1/4)*rate #Digital IF for Up/DownConversion
#timeDiff = np.zeros(1000); #Store time it took to filter the signal

if mode == "tx":
    #print("Finding Average Filter time")
    #for i in range(0,1000) :
    #Place bits to transmit into buffer
    bitsBuffer = random.getrandbits(3000)-random.getrandbits(120)+random.getrandbits(16) #random bits.
    #Modulate signal
    modulatedSig = modulate(bitsBuffer,modType,modOrder)
    #Add Gaussian Noise for Problem 2 Part 1
    #noisySig = modulatedSig + np.random.normal(0,0.02,len(modulatedSig)) + 1j*np.random.normal(0,0.02,len(modulatedSig))
    #Upsample signal
    # upSampledSig = np.repeat(modulatedSig,resampleFactor)
    # rate = rate*resampleFactor #New sampling rate
    
    #Filter the Signal
    #LPF18
    #filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/Lab3/tfLPF18.mat')['tfLPF18']
    #LPF36
    #filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/Lab3/tfLPF36.mat')['tfLPF36']
    #BPF18
    #filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/Lab3/tfBPF18.mat')['tfBPF18']
    #BPF50
    #filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/Lab3/tfBPF50.mat')['tfBPF50']
    #rrcos
    filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/Lab3/rrcos01.mat')['tfrrcos']
    
    #timeBefore = time.time()
    #filterOut = np.convolve(upSampledSig,filterTF[0,:])
    filterOut = np.convolve(modulatedSig,filterTF[0,:])
    #timeDiff[i] = time.time()-timeBefore
    
    #verageFilterTime = np.average(timeDiff)
    
    #print("Average time to run filter: %s s" % averageFilterTime)
    
    #UpConvert signal
    #stime = np.linspace(0,1/digitalIF,int((1/digitalIF)*rate*resampleFactor))
    period = int((1/digitalIF)*rate);
    stime = np.linspace(0,1/digitalIF,len(filterOut))
    upConvertSinusoid = np.exp(1j*2*np.pi*digitalIF*stime*np.floor(len(filterOut)/period))
    upConvertedSig = filterOut*upConvertSinusoid
    
    #Declare with signal to transmit
    sig = upConvertedSig
    nsamps = len(sig)
    
    #Downconversion and filtering as if receiver
    downConvertSinusoid = np.exp(-1j*2*np.pi*digitalIF*stime*np.floor(len(filterOut)/period))
    downConvertedSig = sig*downConvertSinusoid
    sig = np.convolve(downConvertedSig,filterTF[0,:])
    
    
    #DEBUGGING: Plotting for Transmitted Signal
    #Plot Time-Domain
    plt.figure()
    plt.title('Time-Domain of Transmitted Signal')
    plt.xlabel('Sample Number')
    plt.ylabel('Sample Value')
    plt.stem((0.5/np.max(np.abs(np.real(sig))))*np.real(sig[128*2:-128*2]))
    #plt.plot(np.imag(sig))
    plt.stem(np.real(modulatedSig),markerfmt='g')
    plt.show()
    
    #Plot Constellation
    plt.scatter(sig.real,sig.imag)
    plt.title("Transmitted Signal Constellation")
    plt.xlabel("In-Phase")
    plt.ylabel('Quadrature')
    plt.show()
    
    #Plot PSD
    plt.figure()
    plt.title('PSD of Transmitted Signal')
    plt.psd(sig,len(sig),rate)
    plt.show()
    
    # plt.figure()
    # plt.title('PSD of Transmitted Signal')
    # plt.xlabel('Frequency (Hz)')
    # plt.ylabel('Power Spectral Desnity (dB/Hz)')
    # fVals = np.arange(start = -len(sig)/2,stop = len(sig)/2)*rate/len(sig)
    # yf = fftshift(fft(sig))
    # plt.plot(fVals,10*np.log10(yf*np.conj(yf)))
    # plt.grid('both')
    # plt.show()

    
    #Init Tx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    sdr.setSampleRate(SOAPY_SDR_TX, chan, rate)
    sdr.setFrequency(SOAPY_SDR_TX, chan, "RF", freq)
    sdr.setGain(SOAPY_SDR_TX, chan, gain)
    sdr.setAntenna(SOAPY_SDR_TX, chan, "TRX")
    
    #Init Tx Streams
    txStream = sdr.setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32, [0], {})
    ts = sdr.getHardwareTime() + delay #give us delay in nanosec to set everything up.
    txFlags = SOAPY_SDR_HAS_TIME
    sdr.activateStream(txStream)
    
    t=time.time()
    total_samps = 0
    
    #First Transmission needs a time stamp.
    sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), txFlags, ts+delay)
    if sr.ret != len(sig):
            print("Bad Write!!!")
    #continuously transmit until the script is killed
    while True :
        sr = sdr.writeStream(txStream, [sig.astype(np.complex64)], len(sig), 0)
        if sr.ret != len(sig):
            print("Bad Write!!!")
        
        #print an update
        total_samps += len(sig)
        if time.time() - t > 1:
            t=time.time()
            print("Total Samples Sent: %i" % total_samps)
elif mode == "rx":
    nsamps = 8092 #Number of samples
    # Init Rx
    sdr = SoapySDR.Device(dict(driver="iris", serial = serial))
    sdr.setSampleRate(SOAPY_SDR_RX, 0, rate)
    sdr.setFrequency(SOAPY_SDR_RX, 0, "RF", freq)
    sdr.setGain(SOAPY_SDR_RX, 0, gain)
    sdr.setAntenna(SOAPY_SDR_RX, 0, "RX")
    
    #Init buffers
    sampsRecv = np.zeros(nsamps, dtype=np.complex64)
    
    #Init Streams
    rxStream = sdr.setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, [0], {})
    ts = sdr.getHardwareTime() + delay
    sdr.activateStream(rxStream, SOAPY_SDR_HAS_TIME, ts)
    
    t=time.time()
    total_samps = 0
    
    #Receive one packet
    sr = sdr.readStream(rxStream, [sampsRecv], nsamps, timeoutUs= delay)    
    if sr.ret != nsamps:
    	print("Bad Read!!!")
    
    #print an update
    total_samps += nsamps
    print("Total Samples Received: %i" % total_samps)
    #do some rx processing here
    
    #DC Offset Correction
    sampsRecv = (np.real(sampsRecv)-np.average(np.real(sampsRecv))) + 1j*(np.imag(sampsRecv)-np.average(np.imag(sampsRecv)))
    
    #IQ Imbalance Correction
    sampsRecv = alpha*np.real(sampsRecv) + beta*1j*np.imag(sampsRecv)
    
    #Downconvert samples
    #Note: Not time synchronized
    period = period = int((1/digitalIF)*rate);
    stime = np.linspace(0,1/digitalIF,nsamps)
    downConvertSinusoid = np.exp(-1j*2*np.pi*digitalIF*stime*np.floor(nsamps/period))
    sampsRecv = sampsRecv*downConvertSinusoid
    
    #Filter
    #rrcos
    filterTF = scipy.io.loadmat('C:/Users/jam37/Downloads/Lab3/rrcos01.mat')['tfrrcos']
    filterOut = np.convolve(sampsRecv,filterTF[0,:])
    sampsRecv = filterOut
    
    #Downsample samples
    # sampsRecv = sampsRecv[::resampleFactor]
    
    #TODO: Demodulation is harder, so we do that later...
    
    #Plot I/Q in time domain
    plt.figure()
    plt.plot(np.real(sampsRecv))
    plt.plot(np.imag(sampsRecv))
    plt.title("Time Domain of uncorrected 4QAM")
    plt.show()
    
    #Plot I/Q constellation
    plt.figure()
    plt.title("Constellation of uncorrected 4QAM")
    plt.xlabel("In-Phase")
    plt.ylabel("Quadrature")
    plt.scatter(np.real(sampsRecv), np.imag(sampsRecv))
    plt.show()
    
    # #Plot FFT
    # plt.figure()
    # fVals = np.arange(start = -nsamps/2,stop = nsamps/2)*rate/nsamps
    # yf = fftshift(fft(sampsRecv,nsamps))
    # plt.plot(fVals,np.abs(yf),'b')
    # plt.show()
    
    #Plot PSD
    plt.figure()
    plt.title("PSD of uncorrected 4QAM")
    plt.xlabel("Frequency")
    plt.ylabel("Power in dBm")
    [Pxx, freqs] = plt.psd(sampsRecv,nsamps,rate,0)
    plt.show()
    
else:
    print("Error, no mode selected")
    sys.exit(-1)